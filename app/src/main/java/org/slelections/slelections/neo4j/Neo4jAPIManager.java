package org.slelections.slelections.neo4j;

import org.slelections.slelections.generalelections.models.GeneralResult;

import org.slelections.slelections.presidentialelections.models.PresidentialResult;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public class Neo4jAPIManager {

//    // PRODUCTION
//    private static final String API_URL = "https://db-s8nzcbo3lekbxx6dy1pf.graphenedb.com:24780/slelections/";
//    private static final RequestInterceptor requestInterceptor = new RequestInterceptor() {
//        @Override
//        public void intercept(RequestFacade request) {
//            request.addHeader("Content-Type", "application/json");
//            request.addHeader("Authorization", "Basic c2xlbGVjdGlvbnNfcHJvZDc6VmJYamV5Tm4yN0VFajVISnlqWjc=");
//        }
//    };

    // DEBUG
    private static final String API_URL = "http://staging.sb04.stations.graphenedb.com:24789/slelections/";
    private static final RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Content-Type", "application/json");
            request.addHeader("Authorization", "Basic U1RBR0lORzpQVVRpNkxiM3ZoMURVQnhEY3dpaw==");
        }
    };

    private static final RestAdapter REST_ADAPTER = new RestAdapter.Builder()
            .setEndpoint(API_URL)
            .setRequestInterceptor(requestInterceptor)
            .build();
    private static final Neo4jService REST_API = REST_ADAPTER.create(Neo4jService.class);

    public static Neo4jService getRestApi() {
        return REST_API;
    }

    public interface Neo4jService {

        @GET("/presidential/{year}/all-island-summary")
        PresidentialResult getAllIslandPresidentialResult(@Path("year") int year);

        @GET("/general/{year}/all-island-summary")
        GeneralResult getAllIslandGeneralResult(@Path("year") int year);

        @GET("/presidential/{year}/district-summary/{districtCode}")
        PresidentialResult getPresidentialDistrictSummary(@Path("year") int year, @Path("districtCode") int districtCode, @Query("limit") int limit);

        @GET("/general/{year}/district-summary/{districtCode}")
        GeneralResult getGeneralDistrictSummary(@Path("year") int year, @Path("districtCode") int districtCode, @Query("limit") int limit);
    }

}
