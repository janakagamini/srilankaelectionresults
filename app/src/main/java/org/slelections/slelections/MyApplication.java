package org.slelections.slelections;

import android.app.Application;
import android.util.Log;

import com.facebook.appevents.AppEventsLogger;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import java.util.List;

public class MyApplication extends Application {

    //public static final String PARSE_PUSH_CHANNEL = "resultsProduction";
    public static final String PARSE_PUSH_CHANNEL = "resultsDebug2015";

    @Override
    public void onCreate() {
        super.onCreate();

        Parse.enableLocalDatastore(this);

        Parse.initialize(this, "thURGHHERjowr74jahYktOfueUE3bobq1XAK0ruE", "1AkUErt2AxR8y6wJp2LFMAJFvWJ9o0C16c7GvadY");

        ParsePush.subscribeInBackground(PARSE_PUSH_CHANNEL);

        AppEventsLogger.activateApp(this);

    }
}
