package org.slelections.slelections.customviews;

import android.content.Context;
import android.os.Build;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;

import org.slelections.slelections.Synchronizer;

public class MyNestedScrollView extends NestedScrollView implements Synchronizer.Synchronizable {

    public MyNestedScrollView(Context context) {
        super(context);
        init();
    }

    public MyNestedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyNestedScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        // init your ScrollView with style and stuff and then register it

        Synchronizer.getInstance().registerSynchronizable(this);
        final int lastUpdate = Synchronizer.getInstance().getLastUpdate();

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT <= 15) {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                scrollTo(0, lastUpdate);
            }
        });

    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        // listen for "your" scrolls and notify the Synchronizer
        Synchronizer.getInstance().update(this, getScrollY());
    }

    @Override
    public void onUpdate(int update) {
        // listen for the scrolls of your "siblings" from the Synchronizer
        scrollTo(0, update);
    }
}