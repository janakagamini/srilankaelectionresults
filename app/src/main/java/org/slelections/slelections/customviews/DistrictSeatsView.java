package org.slelections.slelections.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Pair;

import org.slelections.slelections.R;

import java.util.ArrayList;

/**
 * TODO: document your custom view class.
 */
public class DistrictSeatsView extends SeatsView {

    //Paint for predicted seat
    private Paint mPaintStroke;

    //Paint for assigned seat
    private Paint mPaintFill;

    //Paint for default seat;
    private Paint mPaintDefault;

    //Externally configurable parameters
    private int seatsCount;
    private int circleSize;
    private int circlePadding;
    private int circleStrokeWidth;
    private ArrayList<Pair<Integer, Boolean>> results;

    private int maxCirclesPerLine;
    private int lines;

    private RectF circle;

    public DistrictSeatsView(Context context) {
        super(context);
        init(null, 0);
    }

    public DistrictSeatsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public DistrictSeatsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.SeatsView, defStyle, 0);

        // Initialize attributes
        seatsCount = a.getInt(R.styleable.SeatsView_seats_count, 1);
        circleSize = a.getDimensionPixelSize(R.styleable.SeatsView_circle_size, 30);
        circlePadding = a.getDimensionPixelSize(R.styleable.SeatsView_circle_padding, 10);
        circleStrokeWidth = a.getDimensionPixelOffset(R.styleable.SeatsView_circle_strokeWidth, 2);
        int defaultCircleColor = a.getColor(R.styleable.SeatsView_circle_defaultColor, Color.WHITE);

        a.recycle();

        // Init paint for circle stroke (used for predicted seats)
        mPaintStroke = new Paint();
        mPaintStroke.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintStroke.setStyle(Paint.Style.STROKE);
        mPaintStroke.setStrokeWidth(circleStrokeWidth);

        // Init paint for circle fill
        mPaintFill = new Paint();
        mPaintFill.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintFill.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaintFill.setShadowLayer(2f, 2f, 2f, Color.LTGRAY);

        // Init paint for default circle fill
        mPaintDefault = new Paint();
        mPaintDefault.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintDefault.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaintDefault.setColor(defaultCircleColor);
        mPaintDefault.setShadowLayer(2f, 2f, 2f, Color.LTGRAY);


        setLayerType(LAYER_TYPE_SOFTWARE, null);

        // Init all seats to defaults
        results = new ArrayList<>(seatsCount);
        for (int i = 0; i < seatsCount; i++) {
            results.add(new Pair<>(Color.WHITE, false));
        }

        circle = new RectF();
    }

    public void setSeats(int seatsCount, ArrayList<Pair<Integer, Boolean>> results) {
        this.seatsCount = seatsCount;
        this.results = results;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int usableWidth = widthSize - getPaddingLeft() - getPaddingRight();
        maxCirclesPerLine = (int)Math.floor(usableWidth / (double) (circleSize + circlePadding));
        lines = (int)Math.ceil(seatsCount / (double)maxCirclesPerLine);
        int heightSize = ((circleSize + circlePadding) * lines) + getPaddingTop() + getPaddingBottom() - circlePadding;

        setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int drawCount = 0;

        finishDrawing:
        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < maxCirclesPerLine; j++) {

                int left = circleStrokeWidth + getPaddingLeft() + (circleSize * j) + (circlePadding * j);
                int top = circleStrokeWidth + getPaddingTop() + (circleSize * i) + (circlePadding * i);
                int right = left + circleSize;
                int bottom = top + circleSize;
                circle.set(left, top, right, bottom);
                try {
                    Pair<Integer, Boolean> result = results.get(drawCount);
                    int color = result.first;
                    boolean assignedSeat = result.second;
                    if (assignedSeat) {
                        mPaintFill.setColor(color);
                        canvas.drawOval(circle, mPaintFill);
                    } else {
                        canvas.drawOval(circle, mPaintDefault);
                        mPaintStroke.setColor(color);
                        canvas.drawOval(circle, mPaintStroke);
                    }
                } catch (IndexOutOfBoundsException e) {
                    canvas.drawOval(circle, mPaintDefault);
                }

                drawCount++;

                if (drawCount >= seatsCount) break finishDrawing;
            }
        }
    }
}
