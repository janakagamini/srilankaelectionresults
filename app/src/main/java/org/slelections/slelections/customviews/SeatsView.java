package org.slelections.slelections.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public abstract class SeatsView extends View {
    public SeatsView(Context context) {
        super(context);
    }

    public SeatsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SeatsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
