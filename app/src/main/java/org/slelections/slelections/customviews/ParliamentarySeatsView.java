package org.slelections.slelections.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;

import org.slelections.slelections.R;
import org.slelections.slelections.utils.MapUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ParliamentarySeatsView extends SeatsView {

    //Paint for predicted seat
    private Paint mPaintStroke;

    //Paint for assigned seat
    private Paint mPaintFill;

    //Paint for default seat;
    private Paint mPaintDefault;

    //Externally configurable parameters
    private int seatsCount;
    private int circleSize;
    private int circlePadding;

    private RectF speakerSeat;
    private ArrayList<RectF> govtSeats;
    private ArrayList<RectF> opposSeats;

    Map<Integer, Integer> assignedSeats;
    Map<Integer, Integer> predictedSeats;


    public ParliamentarySeatsView(Context context) {
        super(context);
        init(null, 0);
    }

    public ParliamentarySeatsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ParliamentarySeatsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.SeatsView, defStyle, 0);

        // Initialize attributes
        seatsCount = a.getInt(R.styleable.SeatsView_seats_count, 225);
        if (seatsCount < 3) seatsCount = 3;
        if (seatsCount % 2 == 0) seatsCount += 1;
        circleSize = a.getDimensionPixelSize(R.styleable.SeatsView_circle_size, 30);
        circlePadding = a.getDimensionPixelSize(R.styleable.SeatsView_circle_padding, 10);
        int circleStrokeWidth = a.getDimensionPixelOffset(R.styleable.SeatsView_circle_strokeWidth, 2);
        int defaultCircleColor = a.getColor(R.styleable.SeatsView_circle_defaultColor, Color.WHITE);

        a.recycle();

        // Init paint for circle stroke (used for predicted seats)
        mPaintStroke = new Paint();
        mPaintStroke.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintStroke.setStyle(Paint.Style.STROKE);
        mPaintStroke.setStrokeWidth(circleStrokeWidth);

        // Init paint for circle fill
        mPaintFill = new Paint();
        mPaintFill.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintFill.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaintFill.setShadowLayer(2f, 2f, 2f, Color.LTGRAY);

        // Init paint for default circle fill
        mPaintDefault = new Paint();
        mPaintDefault.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintDefault.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaintDefault.setColor(defaultCircleColor);
        mPaintDefault.setShadowLayer(2f, 2f, 2f, Color.LTGRAY);
        setLayerType(LAYER_TYPE_SOFTWARE, null);

        // Setup seats
        speakerSeat = new RectF();

        int seatsPerSide = (seatsCount - 1) / 2;
        govtSeats = new ArrayList<>(seatsPerSide);
        opposSeats = new ArrayList<>(seatsPerSide);

        for (int i = 0; i < seatsPerSide; i++) {
            govtSeats.add(new RectF());
            opposSeats.add(new RectF());
        }

        assignedSeats = new HashMap<>();
        predictedSeats = new HashMap<>();
    }

    public void setSeats(int seatsCount, Map<Integer, Integer> assignedSeats, Map<Integer, Integer> predictedSeats) {
        this.seatsCount = seatsCount;
        this.assignedSeats = MapUtils.sortByComparatorInt(assignedSeats, true);
        this.predictedSeats = predictedSeats;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int usableWidth = widthSize - getPaddingLeft() - getPaddingRight() - circlePadding - circleSize;
        int maxCirclesPerLine = usableWidth / (circleSize + circlePadding);
        int halfSeatCount = (seatsCount - 1) / 2;
        int overflowCircles = 0;
        if (halfSeatCount > maxCirclesPerLine) overflowCircles = halfSeatCount % maxCirclesPerLine;

        int lines = ((int) Math.ceil(halfSeatCount / (double) maxCirclesPerLine) * 2) + 1;
        int heightSize = ((circleSize + circlePadding) * lines) + getPaddingBottom() + getPaddingTop() - circlePadding;

        // Position speaker seat
        int left = getPaddingLeft();
        int top = getPaddingTop() + (((lines - 1) / 2) * (circleSize + circlePadding));
        int right = left + circleSize;
        int bottom = top + circleSize;
        speakerSeat.set(left, top, right, bottom);


        int originLeft = (int) speakerSeat.left + circlePadding + circleSize;
        int originTopGov = (int) speakerSeat.top - circlePadding - circleSize;
        int originTopOppos = (int) speakerSeat.top + circlePadding + circleSize;

        int govtSeatCount = 0;

        int linesPerSide = (lines - 1) / 2;
        int lastLineDrawCount = 0;

        int lineCounter = maxCirclesPerLine;
        if (halfSeatCount < maxCirclesPerLine) lineCounter = halfSeatCount;
        for (int i = 0; i < lineCounter; i++) {
            for (int j = 0; j < linesPerSide; j++) {
                if (overflowCircles != 0) {
                    if (j == linesPerSide - 1) {
                        if (lastLineDrawCount >= overflowCircles) continue;
                        else lastLineDrawCount++;
                    }
                }
                int _left = originLeft + ((circleSize + circlePadding) * i);
                int _topGov = originTopGov - ((circleSize + circlePadding) * j);
                int _topOppos = originTopOppos + ((circleSize + circlePadding) * j);
                int _right = _left + circleSize;
                int _bottomGov = _topGov + circleSize;
                int _bottomOppos = _topOppos + circleSize;
                govtSeats.get(govtSeatCount).set(_left, _topGov, _right, _bottomGov);
                opposSeats.get(govtSeatCount).set(_left, _topOppos, _right, _bottomOppos);
                govtSeatCount++;
            }
        }

        setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //Colour speaker seat
        int key = 0;
        int value = 0;
        for (Map.Entry<Integer, Integer> entry : assignedSeats.entrySet()) {
            key = entry.getKey();
            value = entry.getValue();
            mPaintFill.setColor(key);
            canvas.drawOval(speakerSeat, mPaintFill);
            break;
        }

        assignedSeats.put(key, value - 1);

        int govtCount = 0;
        int opposCount = 0;
        int halfSeatCount = (seatsCount - 1) / 2;
        for (Map.Entry<Integer, Integer> entry : assignedSeats.entrySet()) {
            int color = entry.getKey();
            int numOfSeats = entry.getValue();
            int emptyGovt = halfSeatCount - govtCount;
            int emptyOppos = halfSeatCount - opposCount;
            if (emptyGovt >= emptyOppos) {
                // fill govt first
                if (numOfSeats <= emptyGovt) {
                    // enough space in govt side
                    for (int i = 0; i < numOfSeats; i++) {
                        mPaintFill.setColor(color);
                        canvas.drawOval(govtSeats.get(govtCount), mPaintFill);
                        govtCount++;
                    }
                } else {
                    // not enough room
                    for (int i = 0; i < emptyGovt; i++) {
                        mPaintFill.setColor(color);
                        canvas.drawOval(govtSeats.get(govtCount), mPaintFill);
                        govtCount++;
                    }
                    for (int i = 0; i < (numOfSeats - emptyGovt); i++) {
                        mPaintFill.setColor(color);
                        canvas.drawOval(opposSeats.get(opposCount), mPaintFill);
                        opposCount++;
                    }
                }
            } else {
                // fill oppos first
                if (numOfSeats <= emptyOppos) {
                    for (int i = 0; i < numOfSeats; i++) {
                        mPaintFill.setColor(color);
                        canvas.drawOval(opposSeats.get(opposCount), mPaintFill);
                        opposCount++;
                    }
                } else {
                    for (int i = 0; i < emptyOppos; i++) {
                        mPaintFill.setColor(color);
                        canvas.drawOval(opposSeats.get(opposCount), mPaintFill);
                        opposCount++;
                    }
                    for (int i = 0; i < (numOfSeats - emptyOppos); i++) {
                        mPaintFill.setColor(color);
                        canvas.drawOval(govtSeats.get(govtCount), mPaintFill);
                        govtCount++;
                    }
                }
            }
        }

        int leftOverGovt = halfSeatCount - govtCount;
        for (int i = 0; i < leftOverGovt; i++) {
            canvas.drawOval(govtSeats.get(govtCount), mPaintDefault);
            govtCount++;
        }

        int leftOverOppos = halfSeatCount - opposCount;
        for (int i = 0; i < leftOverOppos; i++) {
            canvas.drawOval(opposSeats.get(opposCount), mPaintDefault);
            opposCount++;
        }

//        for (RectF seat : govtSeats) {
//            canvas.drawOval(seat, mPaintDefault);
//        }
//        canvas.drawOval(speakerSeat, mPaintDefault);
//        for (RectF seat : opposSeats) {
//            canvas.drawOval(seat, mPaintDefault);
//        }
    }
}
