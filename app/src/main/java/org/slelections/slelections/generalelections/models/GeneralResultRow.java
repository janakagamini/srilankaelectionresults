package org.slelections.slelections.generalelections.models;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class GeneralResultRow {

    DecimalFormat df = new DecimalFormat("#0.00");

    String partyName;
    String partyColor;
    int votes;
    double percentage;
    double nettPercentage;
    int seats;
    int predictedSeats;
    int nettSeats;

    public int getSeats() {
        return seats;
    }

    public int getNettSeats() {
        return nettSeats;
    }

    public int getPredictedSeats() {
        return predictedSeats;
    }

    public String getPartyNameAsAcronym() {
        String acro = "";

        String[] words = partyName.split(" ");

        for(String word : words) {
            acro += word.substring(0, 1);
        }

        return acro;
    }

    public String getPartyColor() {
        return partyColor;
    }

    public String getPercentageAsString() {
        return df.format(percentage) + "%";
    }

    public String getVotesAsString() {
        return NumberFormat.getNumberInstance(Locale.US).format(votes);
        //return Integer.toString(votes);
    }

    public double getPercentage() {
        return percentage;
    }
}
