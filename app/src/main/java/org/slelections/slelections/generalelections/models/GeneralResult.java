package org.slelections.slelections.generalelections.models;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class GeneralResult {

    DecimalFormat df = new DecimalFormat("#0.00");

    String districtName;
    int validVotes;
    int rejectedVotes;
    int resCount;
    int divisionCount;
    int seats;
    int prevYear;

    ArrayList<GeneralResultRow> resultRows;

    public int getPrevYear() {
        return prevYear;
    }

    public int getSeats() {
        return seats;
    }

    public String getDistrictName() {
        return districtName;
    }

    public int getValidVotes() {
        return validVotes;
    }

    public int getRejectedVotes() {
        return rejectedVotes;
    }

    public ArrayList<GeneralResultRow> getResultRows() {
        return resultRows;
    }

    public int getResCount() {
        return resCount;
    }

    public int getDivisionCount() {
        return divisionCount;
    }

    public String getValidVotesAsString() {
        return NumberFormat.getNumberInstance(Locale.US).format(validVotes);
    }

    public String getRejectedVotesAsString() {
        return NumberFormat.getNumberInstance(Locale.US).format(rejectedVotes);
    }

    public String getTotalPolledAsString() {
        return NumberFormat.getNumberInstance(Locale.US).format(validVotes + rejectedVotes);
    }

    public String getValidVotesPercentageAsString() {
        int totalPolled = validVotes + rejectedVotes;
        float percentage = (validVotes / (float)totalPolled) * 100;
        return df.format(percentage) + "%";
    }

    public String getRejectedVotesPercentageAsString() {
        int totalPolled = validVotes + rejectedVotes;
        float percentage = (rejectedVotes / (float)totalPolled) * 100;
        return df.format(percentage) + "%";
    }
}
