package org.slelections.slelections.generalelections;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.philjay.valuebar.ValueBar;

import org.slelections.slelections.GeneralDistrictDetailActivity;
import org.slelections.slelections.MainActivity;
import org.slelections.slelections.R;
import org.slelections.slelections.customviews.DistrictSeatsView;
import org.slelections.slelections.customviews.ParliamentarySeatsView;
import org.slelections.slelections.generalelections.models.GeneralResult;
import org.slelections.slelections.generalelections.models.GeneralResultRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneralResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    GeneralResult allIslandResult;
    List<GeneralResult> generalResults;

    int year;

    public GeneralResultAdapter(int year) {
        this.year = year;
        this.generalResults = new ArrayList<>();
    }

    public void setHeaderResult(GeneralResult allIslandResult) {
        this.allIslandResult = allIslandResult;
        notifyItemInserted(0);
    }

    public void initData() {
        this.generalResults.clear();
        notifyDataSetChanged();
    }

    public void insertData(GeneralResult generalResult) {
        this.generalResults.add(generalResult);
        notifyItemInserted(generalResults.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.general_all_island_summary, parent, false);
            return new GeneralHeaderViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.general_district_summary, parent, false);
            return new GeneralResultViewHolder(v, new GeneralResultViewHolder.OnDistrictViewHolderClick() {
                @Override
                public void onClick(String districtName) {
                    Intent intent = new Intent(parent.getContext(), GeneralDistrictDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("year", year);
                    bundle.putString("districtName", districtName);
                    intent.putExtras(bundle);
                    parent.getContext().startActivity(intent);
                }
            });
        } else {
            throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof GeneralHeaderViewHolder) {
            ((GeneralHeaderViewHolder) holder).bind(allIslandResult);
        } else if (holder instanceof GeneralResultViewHolder) {
            GeneralResult generalResult = generalResults.get(position - 1);
            ((GeneralResultViewHolder) holder).bind(generalResult);
        } else {
            throw new RuntimeException("wrong holder type");
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return TYPE_HEADER;
        else return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        int i = 0;
        if (allIslandResult != null) i = 1;

        return generalResults.size() + i;
    }

    public static class GeneralHeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView reporting;

        private TableLayout tableLayout;
        private TableLayout tableLayoutHeader;
        private LinearLayout tableLayoutSeparator;
        private TextView gainLossLabel;

        private ParliamentarySeatsView seatsView;

        public GeneralHeaderViewHolder(View itemView) {
            super(itemView);

            reporting = (TextView) itemView.findViewById(R.id.reporting);

            tableLayout = (TableLayout) itemView.findViewById(R.id.tableLayout);
            tableLayoutHeader = (TableLayout) itemView.findViewById(R.id.tableLayoutHeader);
            tableLayoutSeparator = (LinearLayout) itemView.findViewById(R.id.tableLayoutSeparator);
            gainLossLabel = (TextView) itemView.findViewById(R.id.gainLossLabel);

            seatsView = (ParliamentarySeatsView) itemView.findViewById(R.id.seatsView);
        }

        public void bind(GeneralResult allIslandResult) {

            int resCount = allIslandResult.getResCount();
            int divCount = allIslandResult.getDivisionCount();

            String s = "Reporting " + resCount + " of " + divCount + " Divisions";
            reporting.setText(s);

            tableLayout.removeAllViews();
            boolean hasPrevYear = false;
            if (allIslandResult.getPrevYear() == 0) gainLossLabel.setVisibility(View.GONE);
            else hasPrevYear = true;

            Map<Integer, Integer> assignedSeatsMap = new HashMap<>();
            Map<Integer, Integer> predictedSeatsMap = new HashMap<>();

            int i = 0;
            for (GeneralResultRow resultRow : allIslandResult.getResultRows()) {

                TableRow tableRow = (TableRow) LayoutInflater.from(tableLayout.getContext()).inflate(R.layout.general_all_island_summary_result_row, null, false);

                TextView partyName = (TextView) tableRow.findViewById(R.id.partyName);
                partyName.setText(resultRow.getPartyNameAsAcronym());

                TextView partySeats = (TextView) tableRow.findViewById(R.id.partySeats);
                TextView gainLoss = (TextView) tableRow.findViewById(R.id.gainLoss);

                boolean shouldAdd = false;

                int seats = resultRow.getSeats();
                if (seats > 0) {
                    shouldAdd = true;
                    partySeats.setText(Integer.toString(seats));

                    String colorString = resultRow.getPartyColor();
                    int color = seatsView.getResources().getColor(R.color.primary_dark);
                    if (colorString != null) color = Color.parseColor(colorString);
                    assignedSeatsMap.put(color, seats);

                    if (hasPrevYear) {
                        int nettSeats = resultRow.getNettSeats();
                        if (nettSeats > 0) {
                            String nettSeatsString = "+" + nettSeats;
                            gainLoss.setTextColor(Color.BLUE);
                            gainLoss.setText(nettSeatsString);
                        } else if (nettSeats < 0) {
                            String nettSeatsString = "" + nettSeats;
                            gainLoss.setTextColor(Color.RED);
                            gainLoss.setText(nettSeatsString);
                        } else {
                            gainLoss.setText("");
                        }
                    } else gainLoss.setVisibility(View.GONE);
                }

                int predictedSeats = resultRow.getPredictedSeats();
                if (predictedSeats > 0) {
                    String colorString = resultRow.getPartyColor();
                    int color = seatsView.getResources().getColor(R.color.primary_dark);
                    if (colorString != null) color = Color.parseColor(colorString);
                    predictedSeatsMap.put(color, predictedSeats);
                }

                if (shouldAdd) {
                    if (i % 2 != 0) tableRow.setBackgroundColor(tableRow.getResources().getColor(R.color.primary_material_light));
                    tableLayout.addView(tableRow);
                    i++;
                }
            }

            if (tableLayout.getChildCount() == 0) {
                tableLayoutHeader.setVisibility(View.GONE);
                tableLayoutSeparator.setVisibility(View.GONE);
            } else {
                tableLayoutHeader.setVisibility(View.VISIBLE);
                tableLayoutSeparator.setVisibility(View.VISIBLE);
            }

            seatsView.setSeats(225, assignedSeatsMap, predictedSeatsMap);
        }
    }

    public static class GeneralResultViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CardView cardView;

        private TextView districtName;
        private TextView reporting;
        private TableLayout tableLayout;
        private DistrictSeatsView seatsView;

        private TextView absoluteValidVotes;
        private TextView absoluteRejectedVotes;
        private TextView absoluteTotalPolled;

        private TextView percentageValidVotes;
        private TextView percentageRejectedVotes;

        OnDistrictViewHolderClick mListener;

        GeneralResult generalResult;

        public GeneralResultViewHolder(View itemView, OnDistrictViewHolderClick mListener){
            super(itemView);

            this.mListener = mListener;

            cardView = (CardView)itemView.findViewById(R.id.cardView);

            districtName = (TextView) itemView.findViewById(R.id.districtName);
            reporting = (TextView) itemView.findViewById(R.id.reporting);
            tableLayout = (TableLayout) itemView.findViewById(R.id.tableLayout);
            seatsView = (DistrictSeatsView) itemView.findViewById(R.id.seatsView);

            absoluteValidVotes = (TextView) itemView.findViewById(R.id.absoluteValidVotes);
            absoluteRejectedVotes = (TextView) itemView.findViewById(R.id.absoluteRejectedVotes);
            absoluteTotalPolled = (TextView) itemView.findViewById(R.id.absoluteTotalPolled);

            percentageValidVotes = (TextView) itemView.findViewById(R.id.percentageValidVotes);
            percentageRejectedVotes = (TextView) itemView.findViewById(R.id.percentageRejectedVotes);

        }

        public void bind(GeneralResult generalResult) {

            this.generalResult = generalResult;

            cardView.setOnClickListener(this);

            districtName.setText(generalResult.getDistrictName() + " District " + "(" + generalResult.getSeats() + " Seats)");

            int resCount = generalResult.getResCount();
            int divCount = generalResult.getDivisionCount();

            String s;
            if (resCount >= divCount) {
                s = "Reporting " + resCount + " of " + divCount + " Divisions";
            } else {
                s = "Reporting " + resCount + " of " + divCount + " Divisions";
                s += "\n*Estimated seats based on incomplete results";
            }
            reporting.setText(s);

            int districtSeats = generalResult.getSeats();

            ArrayList<GeneralResultRow> resultRows = generalResult.getResultRows();
            tableLayout.removeAllViews();

            ArrayList<Pair<Integer, Boolean>> results = new ArrayList<>();

            for (GeneralResultRow resultRow : resultRows) {
                TableRow tableRow = (TableRow) LayoutInflater.from(tableLayout.getContext()).inflate(R.layout.general_district_summary_result_row, null, false);

                TextView partyName = (TextView) tableRow.findViewById(R.id.partyName);
                partyName.setText(resultRow.getPartyNameAsAcronym());

                TextView partyPercentages = (TextView) tableRow.findViewById(R.id.partyPercentage);
                partyPercentages.setText(resultRow.getPercentageAsString());

                ValueBar valueBar = (ValueBar) tableRow.findViewById(R.id.partyValueBar);
                valueBar.setMinMax(0, 100);
                valueBar.setValue(0);
                valueBar.setDrawValueText(false);
                valueBar.setDrawBorder(false);
                valueBar.setDrawMinMaxText(false);
                valueBar.setTouchEnabled(false);
                valueBar.setBackgroundColor(itemView.getResources().getColor(R.color.primary_light));

                String partyColor = resultRow.getPartyColor();
                int color = itemView.getResources().getColor(R.color.primary_dark);
                if (partyColor != null) color = Color.parseColor(partyColor);
                valueBar.setColor(color);

                Pair<Integer, Boolean> pair = new Pair<>(color, (generalResult.getResCount() >= generalResult.getDivisionCount()));

                for (int i = 0; i < resultRow.getSeats(); i++) {
                    results.add(pair);
                }

                valueBar.animateUp((float) resultRow.getPercentage(), 500);

                TextView partyVotes = (TextView) tableRow.findViewById(R.id.partyVote);
                partyVotes.setText(resultRow.getVotesAsString());

                TextView partySeats = (TextView) tableRow.findViewById(R.id.partySeats);
                if (resCount >= divCount) partySeats.setText(Integer.toString(resultRow.getSeats()));
                else partySeats.setText(resultRow.getSeats() + "*");

                TextView gainLoss = (TextView) tableRow.findViewById(R.id.gainLoss);
                if (generalResult.getPrevYear() > 0) {
                    int gain = resultRow.getNettSeats();
                    if (gain < 0) {
                        gainLoss.setTextColor(Color.RED);
                        gainLoss.setText(Integer.toString(resultRow.getNettSeats()));
                    } else if (gain == 0){
                        gainLoss.setText("-");
                    } else {
                        gainLoss.setTextColor(Color.BLUE);
                        gainLoss.setText("+" + resultRow.getNettSeats());
                    }

                }
                else gainLoss.setVisibility(View.GONE);

                tableLayout.addView(tableRow);
            }

            seatsView.setSeats(districtSeats, results);

            absoluteValidVotes.setText(generalResult.getValidVotesAsString());
            absoluteRejectedVotes.setText(generalResult.getRejectedVotesAsString());
            absoluteTotalPolled.setText(generalResult.getTotalPolledAsString());

            percentageValidVotes.setText(generalResult.getValidVotesPercentageAsString());
            percentageRejectedVotes.setText(generalResult.getRejectedVotesPercentageAsString());
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(generalResult.getDistrictName());
        }

        public static interface OnDistrictViewHolderClick {
            void onClick(String districtName);
        }
    }
}
