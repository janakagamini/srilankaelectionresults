package org.slelections.slelections;

public interface OnLoadMoreListener {
    void onLoadMore();
}
