package org.slelections.slelections;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;

import org.slelections.slelections.generalelections.GeneralElectionFragment;
import org.slelections.slelections.presidentialelections.PresidentialElectionFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements GeneralElectionFragment.OnFragmentInteractionListener, PresidentialElectionFragment.OnFragmentInteractionListener {

    private static final String SHARED_PREFERENCES = "myPreferences";
    private static final String SP_IS_FIRST_START = "isFirstStart";

    DrawerLayout drawerLayout;
    Toolbar toolbar;
    TabLayout tabLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigationView;

    ViewPager viewPager;
    PresidentialAdapter presidentialAdapter;
    GeneralAdapter generalAdapter;

    private void setupNavigationView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout, R.string.hello_world, R.string.hello_world);
        drawerLayout.setDrawerListener(drawerToggle);

        navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.getMenu().getItem(1).setChecked(true);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                int id = menuItem.getItemId();

                drawerLayout.closeDrawer(GravityCompat.START);
                switch (id) {
                    case R.id.presidentialElections:
                        Synchronizer.getInstance().resetToZero();
                        viewPager.setAdapter(presidentialAdapter);
                        viewPager.invalidate();
                        tabLayout.setupWithViewPager(viewPager);
                        getSupportActionBar().setTitle("Presidential Elections");
                        break;
                    case R.id.generalElections:
                        Synchronizer.getInstance().resetToZero();
                        getSupportActionBar().setTitle("General Elections");
                        viewPager.setAdapter(generalAdapter);
                        viewPager.invalidate();
                        tabLayout.setupWithViewPager(viewPager);
                        break;
                    case R.id.about:
                        Toast.makeText(MainActivity.this, "The Grey Lions", Toast.LENGTH_LONG).show();
                        break;
                }
                return false;
            }
        });


    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);

        // Show menu icon
        final ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("General Elections");
    }

    private void setupViewPager() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        presidentialAdapter = new PresidentialAdapter(getSupportFragmentManager());
        presidentialAdapter.addFrag(2015);
        presidentialAdapter.addFrag(2010);

        generalAdapter = new GeneralAdapter(getSupportFragmentManager());
        generalAdapter.addFrag(2015);
        generalAdapter.addFrag(2010);

        viewPager.setAdapter(generalAdapter);
    }

    private void setupTabLayout() {
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupNavigationView();
        setupToolbar();
        setupViewPager();
        setupTabLayout();

        SharedPreferences sp = MainActivity.this.getSharedPreferences(SHARED_PREFERENCES, 0);
        boolean isFirstStart = sp.getBoolean(SP_IS_FIRST_START, true);
        if (isFirstStart) {
            drawerLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }, 500);
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean(SP_IS_FIRST_START, false);
            e.apply();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item))
            return true;

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public static class GeneralAdapter extends FragmentStatePagerAdapter {

        ArrayList<Integer> years = new ArrayList<>();

        public GeneralAdapter(FragmentManager fm) {
            super(fm);
            years = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return 2;
        }

        public void addFrag(int year) {
            years.add(year);
        }

        @Override
        public Fragment getItem(int position) {
            return GeneralElectionFragment.newInstance(years.get(position));
        }

        @Override
        public String getPageTitle(int position) {
            return Integer.toString(years.get(position));
        }
    }

    public static class PresidentialAdapter extends FragmentStatePagerAdapter {

        ArrayList<Integer> years = new ArrayList<>();

        public PresidentialAdapter(FragmentManager fm) {
            super(fm);
            years = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return 2;
        }

        public void addFrag(int year) {
            years.add(year);
        }

        @Override
        public Fragment getItem(int position) {
            return PresidentialElectionFragment.newInstance(years.get(position));
        }

        @Override
        public String getPageTitle(int position) {
            return Integer.toString(years.get(position));
        }
    }
}
