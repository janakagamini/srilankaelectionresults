package org.slelections.slelections.presidentialelections.models;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class PresidentialResult {

    DecimalFormat df = new DecimalFormat("#0.00");

    private String districtName;
    private int divisionCount;
    private int resCount;
    private int validVotes;
    private int rejectedVotes;
    private int prevYear;
    ArrayList<PresidentialResultRow> resultRows;

    public int getValidVotes() {
        return validVotes;
    }

    public String getDistrictName() {
        return districtName;
    }

    public ArrayList<PresidentialResultRow> getResultRows() {
        return resultRows;
    }

    public int getDivisionCount() {
        return divisionCount;
    }

    public int getResCount() {
        return resCount;
    }

    public String getValidVotesAsString() {
        return NumberFormat.getNumberInstance(Locale.US).format(validVotes);
    }

    public String getRejectedVotesAsString() {
        return NumberFormat.getNumberInstance(Locale.US).format(rejectedVotes);
    }

    public String getTotalPolledAsString() {
        return NumberFormat.getNumberInstance(Locale.US).format(validVotes + rejectedVotes);
    }

    public String getValidVotesPercentageAsString() {
        int totalPolled = validVotes + rejectedVotes;
        float percentage = (validVotes / (float)totalPolled) * 100;
        return df.format(percentage) + "%";
    }

    public String getRejectedVotesPercentageAsString() {
        int totalPolled = validVotes + rejectedVotes;
        float percentage = (rejectedVotes / (float)totalPolled) * 100;
        return df.format(percentage) + "%";
    }
}
