package org.slelections.slelections.presidentialelections.models;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class PresidentialResultRow {

    DecimalFormat df = new DecimalFormat("#0.00");

    private String candidateName;
    private String candidateColor;
    private int votes;
    private double percentage;
    private double nettPercentage;

    public int getVotes() {
        return votes;
    }

    public String getCandidateNameAsAcronym() {
        String acro = "";

        String[] words = candidateName.split(" ");

        for(String word : words) {
            acro += word.substring(0, 1);
        }

        return acro;
    }

    public double getPercentage() {
        return percentage;
    }

    public String getPercentageAsString() {
        return df.format(percentage) + "%";
    }

    public String getVotesAsString() {
        return NumberFormat.getNumberInstance(Locale.US).format(votes);
    }

    public String getCandidateColor() {
        return candidateColor;
    }
}
