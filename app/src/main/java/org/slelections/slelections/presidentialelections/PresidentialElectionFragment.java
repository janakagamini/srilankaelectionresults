package org.slelections.slelections.presidentialelections;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.slelections.slelections.R;
import org.slelections.slelections.neo4j.Neo4jAPIManager;
import org.slelections.slelections.presidentialelections.models.PresidentialResult;

import retrofit.RetrofitError;

public class PresidentialElectionFragment extends Fragment {

    private final static String YEAR = "year";
    private int year;

    private PresidentialResultAdapter adapter;
    private View emptyView;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private GetResultTask getResultTask;
    boolean isLoading = false;

    private Handler handler;

    private OnFragmentInteractionListener mListener;

    public static PresidentialElectionFragment newInstance(int year) {
        PresidentialElectionFragment fragment = new PresidentialElectionFragment();
        Bundle args = new Bundle();
        args.putInt(YEAR, year);
        fragment.setArguments(args);
        return fragment;
    }

    public PresidentialElectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.year = getArguments().getInt(YEAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_presidential_election, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        emptyView = v.findViewById(R.id.emptyView);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.accent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isLoading) {
                    getResultTask = new GetResultTask(22);
                    getResultTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                try {
                    int firstPos = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                    if (firstPos > 0) {
                        swipeRefreshLayout.setEnabled(false);
                    } else {
                        swipeRefreshLayout.setEnabled(true);
                        if (recyclerView.getScrollState() == 1)
                            if (swipeRefreshLayout.isRefreshing())
                                recyclerView.stopScroll();
                    }

                } catch (Exception e) {
                    //Log.e("DEBUG", "Scroll Error : " + e.getLocalizedMessage());
                }
            }
        });

        DefaultItemAnimator animator = new DefaultItemAnimator();
        animator.setAddDuration(500);
        animator.setRemoveDuration(500);
        recyclerView.setItemAnimator(animator);

        adapter = new PresidentialResultAdapter();
        recyclerView.setAdapter(adapter);

        handler = new Handler();

        getResultTask = new GetResultTask(22);
        getResultTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (getResultTask != null) {
            if (!getResultTask.isCancelled()) getResultTask.cancel(true);
            getResultTask = null;
        }

        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private class GetResultTask extends AsyncTask<Void, PresidentialResult, Void> {

        int items;

        public GetResultTask(int items) {
            this.items = items;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                final PresidentialResult allIslandResult = Neo4jAPIManager.getRestApi().getAllIslandPresidentialResult(year);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setHeaderResult(allIslandResult);
                    }
                });
            } catch (Exception e) {
                return null;
            }

            for (int i = 1; i <= items; i++) {
                try {
                    final PresidentialResult presidentialResult = Neo4jAPIManager.getRestApi().getPresidentialDistrictSummary(year, i, 2);
                    publishProgress(presidentialResult);
                } catch (RetrofitError error) {
                    if (error.getKind() == RetrofitError.Kind.NETWORK) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Network Error: Please make sure you have an internet connection.", Toast.LENGTH_LONG).show();
                            }
                        });
                        break;
                    }
                }
                if (isCancelled()) break;
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            isLoading = true;

            adapter.initData();

            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            }, 500);
        }

        @Override
        protected void onProgressUpdate(PresidentialResult... presidentialResults) {

            if (presidentialResults[0] != null) adapter.insertData(presidentialResults[0]);

            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (adapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }, 500);

            isLoading = false;
        }

        @Override
        protected void onCancelled(Void aVoid) {

            if (adapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }, 1000);

            isLoading = false;
        }
    }

}
