package org.slelections.slelections.presidentialelections;


import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.philjay.valuebar.ValueBar;

import org.slelections.slelections.R;
import org.slelections.slelections.presidentialelections.models.PresidentialResult;
import org.slelections.slelections.presidentialelections.models.PresidentialResultRow;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PresidentialResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    PresidentialResult allIslandResult;
    List<PresidentialResult> presidentialResults;

    public PresidentialResultAdapter() {
        this.presidentialResults = new ArrayList<>();
    }

    public void setHeaderResult(PresidentialResult allIslandResult) {
        this.allIslandResult = allIslandResult;
        notifyItemInserted(0);
    }

    public void initData() {
        this.presidentialResults.clear();
        notifyDataSetChanged();
    }

    public void insertData(PresidentialResult presidentialResult) {
        this.presidentialResults.add(presidentialResult);
        notifyItemInserted(presidentialResults.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.presidential_all_island_summary, parent, false);
            return new PresidentialHeaderViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.presidential_district_summary, parent, false);
            return new PresidentialResultViewHolder(v);
        } else {
            throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PresidentialHeaderViewHolder) {
            ((PresidentialHeaderViewHolder) holder).bind(allIslandResult);
        } else if (holder instanceof PresidentialResultViewHolder) {
            PresidentialResult presidentialResult = presidentialResults.get(position - 1);
            ((PresidentialResultViewHolder) holder).bind(presidentialResult);
        } else {
            throw new RuntimeException("wrong holder type");
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return TYPE_HEADER;
        else return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        int i = 0;
        if (allIslandResult != null) i = 1;

        return presidentialResults.size() + i;
    }

    public static class PresidentialHeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView reporting;

        private ValueBar rightValueBar;
        private TextView rightName;
        private TextView rightVotes;
        private TextView rightPercentage;


        private ValueBar leftValueBar;
        private TextView leftName;
        private TextView leftVotes;
        private TextView leftPercentage;

        private TextView absoluteValidVotes;
        private TextView absoluteRejectedVotes;
        private TextView absoluteTotalPolled;

        private TextView percentageValidVotes;
        private TextView percentageRejectedVotes;
        //private TextView percentageTotalPolled;


        public PresidentialHeaderViewHolder(View itemView) {
            super(itemView);

            reporting = (TextView) itemView.findViewById(R.id.reporting);

            rightValueBar = (ValueBar) itemView.findViewById(R.id.rightValueBar);
            rightValueBar.setMinMax(0, 100);
            rightValueBar.setValue(100);
            rightValueBar.setDrawValueText(false);
            rightValueBar.setDrawBorder(false);
            rightValueBar.setDrawMinMaxText(false);
            rightValueBar.setTouchEnabled(false);
            rightValueBar.setColor(itemView.getResources().getColor(R.color.primary_light));

            rightName = (TextView) itemView.findViewById(R.id.rightName);
            rightVotes = (TextView) itemView.findViewById(R.id.rightVotes);
            rightPercentage = (TextView) itemView.findViewById(R.id.rightPercentage);

            leftValueBar = (ValueBar) itemView.findViewById(R.id.leftValueBar);
            leftValueBar.setMinMax(0, 100);
            leftValueBar.setValue(0);
            leftValueBar.setDrawValueText(false);
            leftValueBar.setDrawBorder(false);
            leftValueBar.setDrawMinMaxText(false);
            leftValueBar.setTouchEnabled(false);
            leftValueBar.setBackgroundColor(itemView.getResources().getColor(android.R.color.transparent));

            leftName = (TextView) itemView.findViewById(R.id.leftName);
            leftVotes = (TextView) itemView.findViewById(R.id.leftVotes);
            leftPercentage = (TextView) itemView.findViewById(R.id.leftPercentage);

            absoluteValidVotes = (TextView) itemView.findViewById(R.id.absoluteValidVotes);
            absoluteRejectedVotes = (TextView) itemView.findViewById(R.id.absoluteRejectedVotes);
            absoluteTotalPolled = (TextView) itemView.findViewById(R.id.absoluteTotalPolled);

            percentageValidVotes = (TextView) itemView.findViewById(R.id.percentageValidVotes);
            percentageRejectedVotes = (TextView) itemView.findViewById(R.id.percentageRejectedVotes);
            //percentageTotalPolled = (TextView) itemView.findViewById(R.id.percentageTotalPolled);

        }

        public void bind(PresidentialResult allIslandResult) {

            String s = "Reporting " + allIslandResult.getResCount() + " of " + allIslandResult.getDivisionCount() + " Divisions";
            reporting.setText(s);

            PresidentialResultRow leftCandidate = allIslandResult.getResultRows().get(0);

            if (leftCandidate != null) {
                leftName.setText(leftCandidate.getCandidateNameAsAcronym());
                leftVotes.setText(leftCandidate.getVotesAsString());
                leftPercentage.setText(leftCandidate.getPercentageAsString());

                String leftColor = leftCandidate.getCandidateColor();
                if (leftColor != null) leftValueBar.setColor(Color.parseColor(leftColor));
                else leftValueBar.setColor(leftValueBar.getResources().getColor(R.color.primary_dark));
                leftValueBar.animateUp((float) leftCandidate.getPercentage(), 500);
            }

            PresidentialResultRow rightCandidate = allIslandResult.getResultRows().get(1);

            if (rightCandidate != null) {
                rightName.setText(rightCandidate.getCandidateNameAsAcronym());
                rightVotes.setText(rightCandidate.getVotesAsString());
                rightPercentage.setText(rightCandidate.getPercentageAsString());

                String rightColor = rightCandidate.getCandidateColor();
                if (rightColor != null) rightValueBar.setBackgroundColor(Color.parseColor(rightColor));
                else rightValueBar.setBackgroundColor(rightValueBar.getResources().getColor(R.color.primary_dark));
                rightValueBar.animateDown(100 - (float) rightCandidate.getPercentage(), 500);
            }

            absoluteValidVotes.setText(allIslandResult.getValidVotesAsString());
            absoluteRejectedVotes.setText(allIslandResult.getRejectedVotesAsString());
            absoluteTotalPolled.setText(allIslandResult.getTotalPolledAsString());

            percentageValidVotes.setText(allIslandResult.getValidVotesPercentageAsString());
            percentageRejectedVotes.setText(allIslandResult.getRejectedVotesPercentageAsString());


        }
    }

    public static class PresidentialResultViewHolder extends RecyclerView.ViewHolder {

        private TextView districtName;

        private TextView candidate0Name;
        private TextView candidate0Percentage;
        private ValueBar candidate0ValueBar;
        private TextView candidate0Votes;

        private TextView candidate1Name;
        private TextView candidate1Percentage;
        private ValueBar candidate1ValueBar;
        private TextView candidate1Votes;

        private TextView othersPercentage;
        private ValueBar othersValueBar;
        private TextView othersVotes;

        private TextView absoluteValidVotes;
        private TextView absoluteRejectedVotes;
        private TextView absoluteTotalPolled;

        private TextView percentageValidVotes;
        private TextView percentageRejectedVotes;

        public PresidentialResultViewHolder(View itemView) {
            super(itemView);

            districtName = (TextView) itemView.findViewById(R.id.districtName);

            candidate0Name = (TextView) itemView.findViewById(R.id.candidate0Name);
            candidate0Percentage = (TextView) itemView.findViewById(R.id.candidate0Percentage);

            candidate0ValueBar = (ValueBar) itemView.findViewById(R.id.candidate0ValueBar);
            candidate0ValueBar.setMinMax(0, 100);
            candidate0ValueBar.setValue(0);
            candidate0ValueBar.setDrawValueText(false);
            candidate0ValueBar.setDrawBorder(false);
            candidate0ValueBar.setDrawMinMaxText(false);
            candidate0ValueBar.setTouchEnabled(false);
            candidate0ValueBar.setBackgroundColor(itemView.getResources().getColor(R.color.primary_light));

            candidate0Votes = (TextView) itemView.findViewById(R.id.candidate0Votes);

            candidate1Name = (TextView) itemView.findViewById(R.id.candidate1Name);
            candidate1Percentage = (TextView) itemView.findViewById(R.id.candidate1Percentage);

            candidate1ValueBar = (ValueBar) itemView.findViewById(R.id.candidate1ValueBar);
            candidate1ValueBar.setMinMax(0, 100);
            candidate1ValueBar.setValue(0);
            candidate1ValueBar.setDrawValueText(false);
            candidate1ValueBar.setDrawBorder(false);
            candidate1ValueBar.setDrawMinMaxText(false);
            candidate1ValueBar.setTouchEnabled(false);
            candidate1ValueBar.setBackgroundColor(itemView.getResources().getColor(R.color.primary_light));

            candidate1Votes = (TextView) itemView.findViewById(R.id.candidate1Votes);

            othersPercentage = (TextView)itemView.findViewById(R.id.othersPercentage);

            othersValueBar = (ValueBar)itemView.findViewById(R.id.othersValueBar);
            othersValueBar.setMinMax(0, 100);
            othersValueBar.setValue(0);
            othersValueBar.setDrawValueText(false);
            othersValueBar.setDrawBorder(false);
            othersValueBar.setDrawMinMaxText(false);
            othersValueBar.setTouchEnabled(false);
            othersValueBar.setColor(itemView.getResources().getColor(R.color.primary_dark));
            othersValueBar.setBackgroundColor(itemView.getResources().getColor(R.color.primary_light));

            othersVotes = (TextView)itemView.findViewById(R.id.othersVotes);

            absoluteValidVotes = (TextView)itemView.findViewById(R.id.absoluteValidVotes);
            absoluteRejectedVotes = (TextView)itemView.findViewById(R.id.absoluteRejectedVotes);
            absoluteTotalPolled = (TextView)itemView.findViewById(R.id.absoluteTotalPolled);

            percentageValidVotes = (TextView)itemView.findViewById(R.id.percentageValidVotes);
            percentageRejectedVotes = (TextView)itemView.findViewById(R.id.percentageRejectedVotes);
        }

        public void bind(PresidentialResult presidentialResult) {
            districtName.setText(presidentialResult.getDistrictName() + " District");

            PresidentialResultRow candidate0 = presidentialResult.getResultRows().get(0);

            int can0Votes = 0;
            if (candidate0 != null) {
                candidate0Name.setText(candidate0.getCandidateNameAsAcronym());
                candidate0Percentage.setText(candidate0.getPercentageAsString());

                String candidateColor = candidate0.getCandidateColor();
                if (candidateColor != null)
                    candidate0ValueBar.setColor(Color.parseColor(candidateColor));
                else
                    candidate0ValueBar.setColor(itemView.getResources().getColor(R.color.primary_dark));
                candidate0ValueBar.animateUp((float) candidate0.getPercentage(), 500);

                candidate0Votes.setText(candidate0.getVotesAsString());
                can0Votes = candidate0.getVotes();
            }

            PresidentialResultRow candidate1 = presidentialResult.getResultRows().get(1);

            int can1Votes = 0;
            if (candidate1 != null) {
                candidate1Name.setText(candidate1.getCandidateNameAsAcronym());
                candidate1Percentage.setText(candidate1.getPercentageAsString());

                String candidateColor = candidate1.getCandidateColor();
                if (candidateColor != null)
                    candidate1ValueBar.setColor(Color.parseColor(candidateColor));
                else
                    candidate1ValueBar.setColor(itemView.getResources().getColor(R.color.primary_dark));
                candidate1ValueBar.animateUp((float) candidate1.getPercentage(), 500);

                candidate1Votes.setText(candidate1.getVotesAsString());
                can1Votes = candidate1.getVotes();
            }

            int votes = presidentialResult.getValidVotes() - (can0Votes + can1Votes);
            double percentage = (votes / (double)presidentialResult.getValidVotes()) * 100;

            othersVotes.setText(NumberFormat.getNumberInstance(Locale.US).format(votes));

            DecimalFormat df = new DecimalFormat("#0.00");
            othersPercentage.setText(df.format(percentage) + "%");

            othersValueBar.animateUp((float)percentage, 500);


            absoluteValidVotes.setText(presidentialResult.getValidVotesAsString());
            absoluteRejectedVotes.setText(presidentialResult.getRejectedVotesAsString());
            absoluteTotalPolled.setText(presidentialResult.getTotalPolledAsString());

            percentageValidVotes.setText(presidentialResult.getValidVotesPercentageAsString());
            percentageRejectedVotes.setText(presidentialResult.getRejectedVotesPercentageAsString());
        }
    }
}
