package org.slelections.slelections;

import java.util.ArrayList;
import java.util.List;

public class Synchronizer {

    private static Synchronizer instance;
    private static int lastUpdate = 0;

    private List<Synchronizable> synchronizableList;

    private Synchronizer() {}

    public static Synchronizer getInstance() {
        if (instance == null)
            instance = new Synchronizer();
        return instance;
    }

    public void registerSynchronizable(Synchronizable synchronizable) {
        if(synchronizableList == null)
            synchronizableList = new ArrayList<>();

        synchronizableList.add(synchronizable);
    }

    public void update(Synchronizable sender, int update) {
        if(update < 0) update = 0;
        lastUpdate = update;

        for(Synchronizable s : synchronizableList) {
            if(s != sender)
                s.onUpdate(update);
        }
    }

    public void resetToZero() {
        lastUpdate = 0;
    }

    public int getLastUpdate() {
        return lastUpdate;
    }

    public interface Synchronizable {
        public void onUpdate(int update);
    }
}
